using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Barrier : MonoBehaviour
{
    [SerializeField] float cooldown;
    Controller_Player_Car player;
    private bool crash;
    private float timer;
    private void Start()
    {
        crash = false;
        timer = cooldown;
    }
    private void Update()
    {
        if(crash)
        {
            timer -= Time.deltaTime;
            Sound_Manager.instancia.PausarSonido("Car Acceleration");
            if(timer < 0)
            {
                player.forceSpeed = 15;
                timer = cooldown;
                crash = false;
                Destroy(gameObject);
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            player = collision.gameObject.GetComponent<Controller_Player_Car>();
            player.forceSpeed = 0;
            crash = true;
            for(var i = transform.childCount-1; i>= 0; i--)
            {
                transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
}
