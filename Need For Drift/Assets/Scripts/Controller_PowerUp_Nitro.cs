using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PowerUp_Nitro : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            Controller_Player_Car.powerupnitro = true;
            Controller_HUD.hadNitro = true;
            Destroy(gameObject);
        }
    }
}
