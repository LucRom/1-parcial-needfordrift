using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_FinishLine : MonoBehaviour
{
    [SerializeField] GameObject wall;
    [SerializeField] GameObject collisionEnter;
    [SerializeField] GameObject collisionExit;
    private void Start()
    {
        wall.SetActive(true);
    }
    private void Update()
    {
        if(collisionEnter.GetComponent<Controller_DetectionZone_FinishLine>().inZone)
        {
            wall.SetActive(false);
        }
        if (collisionExit.GetComponent<Controller_DetectionZone_FinishLine>().inZone)
        {
            wall.SetActive(true);
            collisionEnter.GetComponent<Controller_DetectionZone_FinishLine>().inZone = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Player")
        {
            Controller_Player_Car.lap++;
        }
    }
}
