using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Controller_Ghost : MonoBehaviour
{
    public Ghost ghost;
    private float time;
    private int index1;
    private int index2;
    void Start()
    {
        time = 0;
    }

    void Update()
    {
        time += Time.unscaledDeltaTime;
        if (ghost.replay)
        {
            GetIndex();
            SetPosition();
        }
    }
    private void GetIndex()
    {
        for (int i=0; i<ghost.keyFrames.Count - 2; i++)
        {
            if (ghost.keyFrames[i] == time)
            {
                index1 = i;
                index2= i;
                break;
            }
            else if (ghost.keyFrames[i] < time & time < ghost.keyFrames[i + 1])
            {
                index1 = i;
                index2= i + 1;
                return;
            }
        }

        index1 = ghost.keyFrames.Count - 1;
        index2 = ghost.keyFrames.Count - 1;
    }
    private void SetPosition()
    {
        if(index1 == index2)
        {
            transform.position = ghost.position[index1];
            transform.eulerAngles = ghost.rotation[index1];
        }
        else
        {
            float interpolation = time - ghost.keyFrames[index1] / ghost.keyFrames[index2] - ghost.keyFrames[index1];

            transform.position = Vector3.Lerp(ghost.position[index1], ghost.position[index2], interpolation);
            transform.eulerAngles = Vector3.Lerp(ghost.rotation[index1], ghost.rotation[index2], interpolation);
        }
    }
}
