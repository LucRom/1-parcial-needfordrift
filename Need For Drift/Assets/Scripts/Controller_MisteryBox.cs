using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_MisteryBox : MonoBehaviour
{
    private Controller_Player_Car player;
    void Start()
    {
        
    }
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            player = other.gameObject.GetComponent<Controller_Player_Car>();
            player.MisteryBox();
            Destroy(gameObject);
        }
    }
}
