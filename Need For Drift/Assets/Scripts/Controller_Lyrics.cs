using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Controller_Lyrics : MonoBehaviour
{
    [SerializeField] TMPro.TMP_Text Lyrics;
    [SerializeField] GameObject btnNext;
    [SerializeField] GameObject btnPrevious;
    private int pageNumer =0;
    private List<string> paragraph = new List<string>();
    void Start()
    {
        paragraph.Add("I wonder if you know how they live in Tokyo (Yes)\r\nIf you seen it, then you mean it, then you know you have to go\r\nFast and Furious (Here it comes, Drift)\r\nFast and Furious (Here it comes, Drift)\r\nWonder if you know how they live in Tokyo (Yes)\r\nIf you seen it, then you mean it, then you know you have to go\r\nFast and Furious (Here it comes, Drift)\r\nFast and Furious (Here it comes, Drift, hey, welcome)");
        paragraph.Add("Hey, welcome! Sorry for the wait!\r\nThe performance of this city full of hustle and bustle\r\nKeep your mouth closed and come with me once\r\nHaving the whole world fascinated, such a extravagant\r\nJAPAN (what?) number one (what?) Jump around\r\nNow, we're up (what?)\r\nTeriyaki Boyz in the place to be\r\nI'll show you, the original VIP");
        paragraph.Add("Many, many diamonds danglin'\r\nBag full of money we stranglin'\r\nHate me, fry me, bake me, try me\r\nAll the above 'cause you can't get in\r\nI don't want?No problem (ah)\r\nBecause me?professional (ah)\r\nMake you shake your ass?Thank you\r\nHaters take it personal");
        paragraph.Add("Like a Kaneda VS Testuo\r\nNeo Tokyo destroyed?\r\nBefore it's gone, the heat\r\nLet's blow it all out, let's go\r\nThe desired mansions, from the Roppongi lab\r\nAn escort \"Eeh?! Amazing\"\r\nI'm not mark 26, Nigo*\r\nInsert \"SOL!\" My pinpoint");
        paragraph.Add("I wonder if you know how they live in Tokyo (Yes)\r\nIf you see me, then you mean it, then you know you have to go\r\nFast and Furious (Here it comes, Drift)\r\nFast and Furious (Here it comes, Drift, [beep])");
        paragraph.Add("Hi! I Am Teriyaki Boyz\r\nBefore you can say \"Ah\" you get blown away by the noise\r\nI'm on the grind, so I'll get what I want\r\nThe pen count is increasing\r\nBasic is a surprise, and\r\nSeriously, every day is dangerous!\r\nNinja-style, like a philosopher but a geisha\r\nAn envoy from the planet of BAPE");
        paragraph.Add("You should see me in the parking lot (ah)\r\n7-ELEVEN is the spot (ah)\r\nBikes with wings and shiny things and\r\nLions, tigers, bears\r\nOh, my ride\r\nWe're Furious and Fast\r\nSupersonic like J. J. Fad, and\r\nWe ride 'til the wheels are flat\r\nCan't beat that with a baseball bat (uh)");
        paragraph.Add("Like a Public Security Section Nine, Togusa**\r\nIf you wait***, good, we'll definitely recruit ASAP\r\nDo an interception,\r\nWorldwide crime is banzai\r\nCome on, hands up\r\nWhen Pharrell drops a bomb\r\nStart dancing, tonight's 4Boyz\r\nFrom far east coast\r\nHow do your murmur (What about my ghost [The car])");
        paragraph.Add("I wonder if you know how they live in Tokyo (Yes)\r\nIf you see me, then you mean it, then you know you have to go\r\nFast and Furious (Here it comes, Drift)\r\nFast and Furious (Here it comes, Drift)");
        paragraph.Add("Yah, Japan, every day is full of adrenaline\r\nHeat island \"Here and there\"\r\nWith a bourgeoning figure \"Doing so-so~\"\r\nInviting me and beckoning \"Over here, over here\"\r\nWith a heat-up, about to dance\r\nSliding around the city, hopping in [the car]\r\nEven if you're one hell of a weirdo, shuffle!!!!\r\nJumbled up, the Fast and Furious");
        paragraph.Add("It's gotta be the shoes, it's gotta be the furs\r\nThat's why ladies choose me76\r\nAll up in the news, 'cause we so cute\r\nThat's why we're so hug-e\r\nHarajuku girls know how I feel\r\nThey respect, I keeps it real�\r\nNot a ?China-man? 'cause I ain't from China, man\r\nI am Japan-man");
        paragraph.Add("I like Tokyo (\"You see me go\")\r\nMy car is nice (\"Coming out of the black Benz SLR\")\r\nYou like Tokyo too (\"I wonder where he get that kind of money?\")\r\nYour car's nice (\"Don't worry about it, let's go\")");
        paragraph.Add("I wonder if you know how they live in Tokyo (Yes)87\r\nIf you seen it, then you mean it, then you know you have to go\r\nFast and Furious (Here it comes, Drift)\r\nFast and Furious (Here it comes, Drift)\r\nWonder if you know how they live in Tokyo (Yes)\r\nIf you seen it, then you mean it, then you know you have to go\r\nFast and Furious (Here it comes, Drift)\r\nFast and Furious (Here it comes, Drift, hey, welcome)");
        Lyrics.text = paragraph[pageNumer];
    }
    void Update()
    {
        if(pageNumer == 0)
        {
            btnNext.SetActive(true);
            btnPrevious.SetActive(false);
        }
        if(pageNumer >= 1)
        {
            btnNext.SetActive(true);
            btnPrevious.SetActive(true);
        }
        if(pageNumer == 13)
        {
            btnNext.SetActive(false);
            btnPrevious.SetActive(true);
        }
    }
    public void NextPage()
    {
        pageNumer++;
        Lyrics.text = paragraph[pageNumer];
    }
    public void PreviousPage()
    {
        pageNumer--;
        Lyrics.text = paragraph[pageNumer];
    }
}
