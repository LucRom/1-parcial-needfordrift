using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Controller_Enemy_RaceCar : MonoBehaviour
{
    [SerializeField] List<Transform> waypoints = new List<Transform>();
    NavMeshAgent agent;
    public float speed;
    public float aceleration;
    public string botname;
    public int lap;
    public int nextwaypoint;
    void Start()
    {
        lap = 1;
        agent = GetComponent<NavMeshAgent>();
        nextwaypoint= 0;
        agent.speed = speed;
        agent.acceleration= aceleration;
    }
    void Update()
    {
        if(agent.remainingDistance < 1)
        {
            NextWaypoint();
            UptadeDestination();
        }
        if(lap == 3)
        {
            Game_Manager.gameOver = true;
        }
    }
    private void UptadeDestination()
    {
        agent.SetDestination(waypoints[nextwaypoint].position);
    }
    private void NextWaypoint()
    {
        nextwaypoint++;
        if(nextwaypoint == waypoints.Count)
        {
            nextwaypoint = 0;
            lap++;
        }
    }
}
