using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_DetectionZone_FinishLine : MonoBehaviour
{
    public bool inZone;
    public bool detectionZoneExit;
    void Start()
    {
        inZone = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        inZone= true;
    }
    private void OnTriggerExit(Collider other)
    {
        if(detectionZoneExit)
        {
            inZone = false;
        }
    }
}
