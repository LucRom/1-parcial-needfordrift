using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Controller_Enemy_Police : MonoBehaviour
{
    [SerializeField] Transform player;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] float speed;
    [SerializeField] float aceleration;
    [Header("invulnerability")]
    private float timerInvunerability;
    public float cooldownInvunerability;
    private bool canAttack;
    void Start()
    {
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
        agent.acceleration = aceleration;
        timerInvunerability = cooldownInvunerability;
        canAttack = true;
    }
    void Update()
    {
        agent.SetDestination(player.position);
        CheckAttack();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Player" && canAttack)
        {
            Controller_Player_Car.life--;
            canAttack = false;
        }
    }
    public void CheckAttack()
    {
        if(canAttack == false)
        {
            timerInvunerability -= Time.deltaTime;
            if (timerInvunerability <= 0)
            {
                canAttack = true;
                timerInvunerability = cooldownInvunerability;
            }
        }
    }
}
