using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class Controller_Player_Car : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] public float forceSpeed;
    [SerializeField] float drag;
    [SerializeField] float maxSpeed;
    [SerializeField] public float maxRotate;
    [SerializeField] float traction;
    private Vector3 forceMove;
    [Header("Data")]
    public static int life;
    public static int lap;
    public static bool drift;
    [Header("Nitro")]
    public static bool powerupnitro;
    private bool nitro;
    public float cooldown;
    private float timer;
    public GameObject nitroParticles;
    [Header("MisteryBox")]
    public GameObject spawnPowerUp1;
    public GameObject spawnPowerUp2;
    public GameObject nitropowerup;
    public GameObject barrier;
    public GameObject spikes;
    public GameObject resetEnemy;
    [Header("Damage")]
    public GameObject damagepanel;
    void Start()
    {
        nitro = false;
        powerupnitro = false;
        timer = cooldown;
        drift = false;
        life = 4;
        lap = 1;
        nitroParticles.SetActive(false);
    }
    void Update()
    {
        Movement();
        Sounds();

        RaycastHit Hit;

        if (Physics.Raycast(transform.position, transform.forward, out Hit, 1.2f))
        {
            if (Hit.transform.CompareTag("Collision") || Hit.transform.CompareTag("Barrier") || Hit.transform.CompareTag("Enemy") || Hit.transform.CompareTag("Race Enemy"))
            {
                forceMove = forceMove.normalized;
            }
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            maxRotate += 20;
            drift = true;
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            drift = false;
            maxRotate -= 20;
        }
        if(Input.GetMouseButtonDown(0) && powerupnitro)
        {
            nitroParticles.SetActive(true);
            Sound_Manager.instancia.ReproducirSonido("Nitro Sfx");
            nitro = true;
        }
        if(nitro)
        {
            Nitro();
        }
        if(life > 2)
        {
            damagepanel.SetActive(false);
        }
        if(life == 2)
        {
            damagepanel.SetActive(true);
            damagepanel.GetComponent<MeshRenderer>().material.SetFloat("_Mask_contrast", 0.8f);
        }
        if (life == 1)
        {
            damagepanel.SetActive(true);
            damagepanel.GetComponent<MeshRenderer>().material.SetFloat("_Mask_contrast", 0.5f);
        }
    }
    private void Movement()
    {
        float moveFowarBackwards = Input.GetAxis("Vertical") * forceSpeed * Time.deltaTime;
        forceMove += transform.forward * moveFowarBackwards;
        transform.position += forceMove * Time.deltaTime;

        forceMove *= drag;
        forceMove = Vector3.ClampMagnitude(forceMove, maxSpeed);

        float rotation = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.up * rotation * forceMove.magnitude * maxRotate * Time.deltaTime);

        forceMove = Vector3.Lerp(forceMove.normalized, transform.forward, traction * Time.deltaTime) * forceMove.magnitude;
    }
    private void Nitro()
    {
        timer -= Time.deltaTime;
        forceSpeed = 30;
        if (timer <= 0 )
        {
            Sound_Manager.instancia.PausarSonido("Nitro Sfx");
            Controller_HUD.hadNitro = false;
            nitroParticles.SetActive(false);
            forceSpeed = 15;
            powerupnitro = false;
            nitro = false;
            timer = cooldown;
        }
    }
    public void MisteryBox()
    {
        int rnd = UnityEngine.Random.Range(0, 4);
        switch (rnd)
        {
            case 0:
                Instantiate(nitropowerup, spawnPowerUp1.transform.position, Quaternion.identity);
                Debug.Log("Nitro");
                break;
            case 1:
                Instantiate(resetEnemy, spawnPowerUp2.transform.position, Quaternion.identity);
                Debug.Log("ResetEnemy");
                break;
            case 2:
                Instantiate(barrier, spawnPowerUp2.transform.position, Quaternion.identity);
                Debug.Log("Barrier");
                break;
            case 3:
                Instantiate(spikes, spawnPowerUp2.transform.position, Quaternion.identity);
                Debug.Log("Spikes");
                break;
        }
    }
    private void Sounds()
    {
        if(Input.GetKeyDown(KeyCode.W))
        {
            Sound_Manager.instancia.ReproducirSonido("Car Acceleration");
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            Sound_Manager.instancia.ReproducirSonido("Car Reverse");
        }
        if(Input.GetKeyUp(KeyCode.W))
        {
            Sound_Manager.instancia.PausarSonido("Car Acceleration");
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            Sound_Manager.instancia.PausarSonido("Car Reverse");
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Sound_Manager.instancia.ReproducirSonido("Drifting Car");
        }
    }

}
