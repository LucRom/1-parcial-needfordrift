using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Controller_Enemy_CivilCar : MonoBehaviour
{
    public GameObject zone;
    private Rigidbody rb;
    private Controller_DetectionZone_Civil detectionZone;
    private int rnd;
    public bool rotate;
    [SerializeField] List<Transform> waypoints = new List<Transform>();
    NavMeshAgent agent;
    [SerializeField] float speed;
    [SerializeField] float aceleration;
    private int nextwaypoint;
    void Start()
    {
        detectionZone = zone.GetComponent<Controller_DetectionZone_Civil>();
        rotate= false;
        agent = GetComponent<NavMeshAgent>();
        nextwaypoint = 0;
        agent.speed = speed;
        agent.acceleration = aceleration;
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        if(detectionZone.inZone)
        {
            rnd = UnityEngine.Random.Range(-50, 51);
            agent.isStopped = true;
            speed = 0;
            aceleration = 0;
            transform.rotation = Quaternion.Euler(0, rnd,0);
            rb.AddForce(Vector3.forward * 3.5f, ForceMode.Impulse);
            rotate = true;
            agent.isStopped = false;
        }
        if (agent.remainingDistance < 1)
        {
            NextWaypoint();
            UptadeDestination();
        }
    }
    private void UptadeDestination()
    {
        agent.SetDestination(waypoints[nextwaypoint].position);
    }
    private void NextWaypoint()
    {
        nextwaypoint++;
        if (nextwaypoint == waypoints.Count)
        {
            nextwaypoint = 0;
        }
    }
}