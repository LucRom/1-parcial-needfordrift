using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Recorder : MonoBehaviour
{
    public Ghost ghost;
    private float timer;
    private float time;

    private void Awake()
    {
        if (ghost.record)
        {
            ghost.ResetRecord();
            timer = 0;
            time = 0;
        }
    }
    void Update()
    {
        timer += Time.unscaledDeltaTime;
        time += Time.unscaledDeltaTime;

        if(ghost.record & timer > 1/ghost.fps)
        {
            ghost.keyFrames.Add(time);
            ghost.position.Add(transform.position);
            ghost.rotation.Add(transform.eulerAngles);

            timer = 0;
        }
    }
}
