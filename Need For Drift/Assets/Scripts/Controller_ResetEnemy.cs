using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_ResetEnemy : MonoBehaviour
{
    private GameObject temp;
    public GameObject spawnpoint;
    private void Start()
    {
        spawnpoint = GameObject.Find("Spawn Race Car");
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Length >= 8 && collision.gameObject.name.Substring(0, 8) == "Race Car")
        {
            temp = collision.gameObject;
            ResetPosition();
        }
    }
    private void ResetPosition()
    {
        temp.transform.position = spawnpoint.transform.position;
        Destroy(gameObject);
    }
}
