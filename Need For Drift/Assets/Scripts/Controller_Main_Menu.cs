using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Controller_Main_Menu : MonoBehaviour
{
    [SerializeField] GameObject btnPlay;
    public bool music;
    public Ghost ghost;
    private bool secondPlay;
    private void Start()
    {
        secondPlay = PersistentManager.instancia.ReturnSecondPlay();
        music = false;
    }
    private void Update()
    {
        if(music)
        {
            btnPlay.SetActive(true);
        }
        else
        {
            btnPlay.SetActive(false);
        }
    }
    public void Back()
    {
        Sound_Manager.instancia.PausarSonido("Tokyo Drift (Fast & Furious)");
    }
    public void Music()
    {
        music= true;
        Sound_Manager.instancia.ReproducirSonido("Tokyo Drift (Fast & Furious)");
    }
    public void Play()
    {
        if(secondPlay == false)
        {
            ghost.ResetRecord();
        }
        SceneManager.LoadScene(1);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
