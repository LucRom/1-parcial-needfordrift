using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Game_Manager : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject playerGhost;
    [SerializeField] GameObject raceEnemy;
    [SerializeField] GameObject raceEnemy2;
    [SerializeField] GameObject policeCar;
    [SerializeField] GameObject civilCar;
    [SerializeField] GameObject misteryBox;
    [SerializeField] GameObject nitroPowerUp;
    [SerializeField] GameObject speedBoost;
    [SerializeField] GameObject enemyReset;
    [SerializeField] GameObject barrier;
    [SerializeField] GameObject spikes;
    [SerializeField] GameObject spawnPointPlayer;
    [SerializeField] GameObject spawnPointEnemy;
    [SerializeField] GameObject spawnPointEnemy2;
    public static bool gameOver;
    private bool win;
    public List<GameObject> spikesSpawnPoints = new List<GameObject>();
    private List<GameObject> items = new List<GameObject>();
    private GameObject currentSpike;
    public Ghost ghost;
    public static bool secondPlay;
    void Start()
    {
        secondPlay = PersistentManager.instancia.ReturnSecondPlay();
        if (secondPlay == false)
        {
            ghost.record = true;
            ghost.replay = false;
        }
        StartGame();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            PersistentManager.instancia.RestartBool();
            StartGame();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Controller_Player_Car.lap = 2;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        if (Controller_Player_Car.life <= 0)
        {
            gameOver = true;
        }
        if(gameOver)
        {
            Time.timeScale = 0;
            Controller_HUD.hudGameOver = true;
        }
        if (win)
        {
            secondPlay = true;
            PersistentManager.instancia.SaveSecondPlay();
            Time.timeScale = 0;
            Controller_HUD.hudWin = true;
        }
        if(Controller_Player_Car.lap == 3)
        {
            win = true;
        }
        if (Controller_Player_Car.lap == 2)
        {
            policeCar.SetActive(true);
        }
        if (secondPlay)
        {
            ghost.record = false;
            ghost.replay = true;
        }
    }
    private void StartGame()
    {
        Time.timeScale = 1;
        Controller_HUD.hudGameOver = false;
        Controller_HUD.hudWin = false;
        Controller_HUD.showControls = true;
        win = false;
        gameOver = false;
        Destroy(currentSpike);
        currentSpike = Instantiate(spikes, spikesSpawnPoints[UnityEngine.Random.Range(0, spikesSpawnPoints.Count)].transform.position, Quaternion.identity);
        Controller_Player_Car.life = 4;
        Controller_Player_Car.lap = 1;
        player.GetComponent<Controller_Player_Car>().maxRotate = 20;
        player.transform.position = spawnPointPlayer.transform.position;
        player.transform.rotation = Quaternion.Euler(0,0,0);
        raceEnemy.GetComponent<Controller_Enemy_RaceCar>().lap = 1;
        raceEnemy.transform.position = spawnPointEnemy.transform.position;
        raceEnemy.transform.rotation = Quaternion.Euler(0, 0, 0);
        raceEnemy2.transform.position = spawnPointEnemy2.transform.position;
        raceEnemy2.transform.rotation = Quaternion.Euler(0, 0, 0);
        raceEnemy2.GetComponent<Controller_Enemy_RaceCar>().lap = 1;
        civilCar.transform.position = new Vector3(17.86f, 1.5f, 75.7f);
        civilCar.transform.rotation = Quaternion.Euler(0, -180, 0);
        policeCar.transform.position = new Vector3(40, 1.5f, 44);
        policeCar.transform.rotation = Quaternion.Euler(0, -165, 0);
        policeCar.SetActive(false);
        foreach (GameObject item in items)
        {
            Destroy(item);
        }
        items.Add(Instantiate(misteryBox, new Vector3(32, 1, 89), Quaternion.Euler(0, 0, 0)));
        items.Add(Instantiate(misteryBox, new Vector3(59, 1, 59), Quaternion.Euler(0, 90, 0)));
        items.Add(Instantiate(nitroPowerUp, new Vector3(15, 1, 34.3f), Quaternion.Euler(0, 0, 0)));
        items.Add(Instantiate(nitroPowerUp, new Vector3(87, 1, 40), Quaternion.Euler(0,0, 0)));
        items.Add(Instantiate(enemyReset, new Vector3(72, 0.5f, 85f), Quaternion.Euler(0, -60, 0)));
        items.Add(Instantiate(speedBoost, new Vector3(85, 0.5f, 57.8f), Quaternion.identity));
        items.Add(Instantiate(barrier, new Vector3(61, 0.75f, 44.49f), Quaternion.Euler(0, 150, 0)));
    }
}
