using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Controller_SpeedBost : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] float cooldown;
    Controller_Player_Car player;
    private bool touch;
    private float timer;
    [Header("Enemy")]
    [SerializeField] float cooldownE;
    NavMeshAgent enemy;
    private bool touchE;
    private float timerE;
    void Start()
    {
        touch = false;
        touchE = false;
        timer = cooldown;
        timerE = cooldownE;
    }
    void Update()
    {
        PlayerTouch();
        EnemyTouch();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            player = collision.gameObject.GetComponent<Controller_Player_Car>();
            player.forceSpeed = 30;
            touch = true;
        }
        if (collision.gameObject.CompareTag("Race Enemy"))
        {
            enemy = collision.gameObject.GetComponent <NavMeshAgent>();
            enemy.acceleration = 28;
            enemy.speed = 20;
            touchE = true;
        }
    }
    private void PlayerTouch()
    {
        if (touch)
        {
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                player.forceSpeed = 15;
                timer = cooldown;
                touch = false;
            }
        }
    }
    private void EnemyTouch()
    {
        if (touchE)
        {
            timerE -= Time.deltaTime;

            if (timerE < 0)
            {
                enemy.speed = 15;
                enemy.acceleration = 20;
                timerE = cooldown;
                touchE = false;
            }
        }
    }
}
