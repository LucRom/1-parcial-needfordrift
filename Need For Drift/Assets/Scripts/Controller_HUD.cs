using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Controller_HUD : MonoBehaviour
{
    [SerializeField] GameObject gameManager;
    [SerializeField] TMPro.TMP_Text laps;
    [SerializeField] TMPro.TMP_Text lifes;
    [SerializeField] TMPro.TMP_Text status;
    [SerializeField] TMPro.TMP_Text nitro;
    [SerializeField] GameObject controls;
    public static bool hudGameOver;
    public static bool hudWin;
    public static bool hadNitro;
    public static bool showControls;
    public float timer;
    public float cooldown;

    void Start()
    {
        timer = cooldown;
        showControls= true;
        hadNitro= false;
        hudGameOver = false;
        hudGameOver = false;
    }
    void Update()
    {
        lifes.text = "Life: "+Controller_Player_Car.life.ToString() + "/4";
        laps.text = "Lap: " + Controller_Player_Car.lap + "/3";
        if (hudGameOver)
        {
            status.text = "You lost \n Press r to restart";
        }
        else if (hudWin)
        {
            status.text = "You Win \n Press r to restart";
        }
        else
        {
            ResetStatusText();
        }
        if (hadNitro)
        {
            nitro.text = "Click izq. para Nitro";
        }
        if(hadNitro == false)
        {
            nitro.text = "";
        }
        if(showControls)
        {
            controls.SetActive(true);
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                showControls = false;
                timer = cooldown;
            }
        }
        if(showControls == false)
        {
            controls.SetActive(false);
        }
    }
    private void ResetStatusText()
    {
        status.text = "";
    }
}
