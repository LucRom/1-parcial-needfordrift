using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Ghost : ScriptableObject
{

    public bool record;
    public bool replay;
    public float fps;

    public List<float> keyFrames;
    public List<Vector3> position;
    public List<Vector3> rotation;

    public void ResetRecord()
    {
        keyFrames.Clear();
        position.Clear();
        rotation.Clear(); 
    }
}
