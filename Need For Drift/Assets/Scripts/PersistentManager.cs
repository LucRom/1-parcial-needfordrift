using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class PersistentManager : MonoBehaviour
{
    public static PersistentManager instancia;
    public DataPersistencia data;
    string archivoDatos = "save.dat";

    private void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

        CargarDataPersistencia();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G)) GuardarDataPersistencia();
    }
    public void SaveSecondPlay()
    {
        data.secondPlay = Game_Manager.secondPlay;
        GuardarDataPersistencia();
    }
    public bool ReturnSecondPlay()
    {
        return data.secondPlay;
    }
    public void RestartBool()
    {
        data.secondPlay = false;
        GuardarDataPersistencia();
    }
    public void GuardarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos guardados");
    }

    public void CargarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();
            Debug.Log("Datos cargados");
        }
    }
}
[System.Serializable]
public class DataPersistencia
{
    public bool secondPlay;
    public DataPersistencia()
    {
        secondPlay = false;
    }
}

