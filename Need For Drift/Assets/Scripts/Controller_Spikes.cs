using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Spikes : MonoBehaviour
{
    private Controller_Player_Car player;
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Controller_Player_Car>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Player")
        {
            player.maxRotate += 10;
            Sound_Manager.instancia.ReproducirSonido("Tire Deflating");
            Controller_Player_Car.life--;
        }
    }
}
