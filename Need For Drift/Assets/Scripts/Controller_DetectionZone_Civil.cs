using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_DetectionZone_Civil : MonoBehaviour
{
    public bool inZone;
    private Controller_Enemy_CivilCar civil;
    void Start()
    {
        inZone= false;
        civil = transform.parent.GetComponent<Controller_Enemy_CivilCar>();
    }
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy")) && civil.rotate == false)
        {
            inZone= true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy"))
        {
            inZone = false;
        }
    }
}
